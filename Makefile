RAYLIB_PATH=/home/kira/opt/raylib-5.0_linux_amd64

all: run

run: main
	LD_LIBRARY_PATH=${RAYLIB_PATH}/lib ./main

main: main.c
	gcc main.c \
		-Wall -Werror \
		-I${RAYLIB_PATH}/include/ \
		-L${RAYLIB_PATH}/lib/ \
		-lraylib -lGL -lm -lpthread -ldl -lrt -lX11 \
		-o main

release: main
	rm -rf build
	mkdir -p build/segeln-sie
	cp main build/segeln-sie/segeln-sie
	cp run.sh build/segeln-sie
	cp *.png build/segeln-sie
	( cd build && tar cvzf segeln-sie-v0.1.0.tar.gz segeln-sie/ )

