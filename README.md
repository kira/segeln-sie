# Segeln Sie!

> A very rudimentary 2D sailing simulator.

*Segeln Sie!* is, roughly, German for "Go sailing!".

## Controls
- `left arrow`: turn port (left)
- `left right`: turn starboard (right)
- `m`: harden mainsail (the big sail)
- `n`: ease mainsail
- `j`: harden jib (the small sail)
- `h`: ease jib
- `-`: zoom out
- `=`: zoom in

## License
MIT

