#include <raylib.h>
#include <rlgl.h>
#include <GL/gl.h>
#include <math.h>
#include <stdio.h>

const float SCALE_PX_TO_M   = 1.0f/40.0f;
const float SCALE_M_TO_PX   = 40.0f;
const float MS_TO_KNOTS     = 1.9438445f;

const float DT              = 1 / 2800.0f;

const float BUBBLE_LIFETIME = 200e2;

typedef enum {
  STARBOARD = 0,
  PORT = 1
} Side;

typedef struct {
  Vector2 pos;
} WindParticle;

Vector2 windDir;

#define MAX_WIND_PARTICLES 128
WindParticle wind[MAX_WIND_PARTICLES];

typedef struct {
  Vector2 pos;
  Vector2 vel;
  int lifetime;
} BubbleParticle;

#define MAX_BUBBLE_PARTICLES 256
BubbleParticle bubbles[MAX_BUBBLE_PARTICLES];

int rand (int min, int max) {
  return GetRandomValue(min, max);
}

float randf (float min, float max) {
  return rand(0, (max-min)*1e6f) / 1e6f + min;
}

float normAngle (float ang) {
  while (ang > 180) ang -= 360;
  while (ang < -180) ang += 360;
  return ang;
}

Vector2 v (float x, float y) {
  return (Vector2){ x, y };
}

Vector2 vadd (Vector2 a, Vector2 b) {
  return (Vector2){ a.x + b.x, a.y + b.y };
}

Vector2 vsub (Vector2 a, Vector2 b) {
  return (Vector2){ a.x - b.x, a.y - b.y };
}

Vector2 vscale (Vector2 a, float c) {
  return (Vector2){ a.x * c, a.y * c };
}

float vlen (Vector2 a) {
  return sqrt(a.x*a.x + a.y*a.y);
}

float vang (Vector2 a) {
  return atan2f(a.y, a.x) * RAD2DEG;
}

Vector2 vdir (float degrees, float length) {
  const float radians = degrees * DEG2RAD;
  return v(cos(radians) * length, sin(radians) * length);
}

Vector2 randInCircle (float radius) {
  float theta = randf(0, 360);
  return vdir(theta, randf(0, radius));
}

void drawWater (Texture2D *texture, Camera2D camera) {
  const float tw = (GetScreenWidth() / texture->width) / camera.zoom;
  const float th = (GetScreenHeight() / texture->height) / camera.zoom;
  const float tx = (-camera.offset.x / texture->width) / camera.zoom \
                   + (camera.target.x / texture->width);
  const float ty = (-camera.offset.y / texture->height) / camera.zoom \
                   + (camera.target.y / texture->height);

  const float x = 0;
  const float y = 0;
  const float w = GetScreenWidth();
  const float h = GetScreenHeight();

  rlSetTexture(texture->id);

  rlBegin(RL_QUADS);
      rlColor4ub(255, 255, 255, 255);
      rlNormal3f(0.0f, 0.0f, 1.0f);

      // Top-left corner
      rlTexCoord2f(tx, ty);
      rlVertex2f(x, y);

      // Bottom-left corner for texture and quad
      rlTexCoord2f(tx, ty + th);
      rlVertex2f(x, y + h);

      // Bottom-right corner for texture and quad
      rlTexCoord2f(tx + tw, ty + th);
      rlVertex2f(x + w, y + h);

      // Top-right corner for texture and quad
      rlTexCoord2f(tx + tw, ty);
      rlVertex2f(x + w, y);
  rlEnd();

  rlSetTexture(0);
}

typedef struct {
  Vector2 pos;
  Vector2 vel;
  float rot;
  float pivotX;
  float massKg;
  float mainHardening;  // 0..1
  float jibHardening;   // 0..1
} Vessel;

void drawTelltales (Vector2 at, Side side, float theta) {
  Vector2 endPos = vadd(at, vdir(theta, 135));
  Vector2 tstartPos = vadd(at, vdir(theta + (side == STARBOARD ? -90 : +90), 4.0f));
  Vector2 downSail = vdir(theta, 1.0f);
  for (int i=0; i < 3; i++) {
    tstartPos = vadd(tstartPos, vscale(downSail, 32.0f));
    endPos = vadd(tstartPos, vscale(downSail, 16.0f));
    // Jitter due to turbulent air flow.
    float jitter = 0;
    endPos = vadd(endPos, vdir(randf(theta-jitter, theta+jitter), 5.0f));
    DrawLineEx(tstartPos, endPos, 1.0f, side == PORT ? RED : GREEN);
  }
}

void drawShip (Texture2D *texture, Vessel *ship) {
  Vector2 drawPos = vscale(ship->pos, SCALE_M_TO_PX);
  drawPos = vadd(drawPos, vdir(ship->rot - 180, 64));
  Rectangle src = { 0, 0, texture->width, texture->height };
  Rectangle dst = { drawPos.x, drawPos.y, texture->width, texture->height };
  Vector2 origin = { ship->pivotX * texture->width, texture->height/2 };

  // Draw drop shadow.
  BeginBlendMode(BLEND_ALPHA);
    Color shadow = { 0, 0, 0, 64 };
    Rectangle shadowDst = { dst.x+8, dst.y+8, dst.width, dst.height };
    DrawTexturePro(*texture, src, shadowDst, origin, ship->rot, shadow);
  EndBlendMode();

  // Draw ship.
  DrawTexturePro(*texture, src, dst, origin, ship->rot, WHITE);

  // Determine tack.
  ship->rot = normAngle(ship->rot);
  int tack = -1;
  if (normAngle(ship->rot - vang(windDir)) > 0) {
    tack = +1;
  }

  // Draw mainsail.
  Vector2 startPos, endPos;
  float mainAngle = ship->rot + 180 + tack * 90 * (1.0f - ship->mainHardening);
  startPos = vadd(drawPos, vdir(ship->rot, 96));
  endPos = vadd(startPos, vdir(mainAngle, 135));
  DrawLineEx(startPos, endPos, 4.0f, BROWN);

  // Draw mainsail telltales.
  drawTelltales(startPos, STARBOARD, mainAngle);
  drawTelltales(startPos, PORT, mainAngle);

  // Draw jib.
  float jibAngle = ship->rot + 175 + tack * 85 * (1.0f - ship->jibHardening);
  startPos = vadd(drawPos, vdir(ship->rot, 240));
  endPos = vadd(startPos, vdir(jibAngle, 150));
  DrawLineEx(startPos, endPos, 4.0f, BROWN);

  // Draw jib telltales.
  drawTelltales(startPos, STARBOARD, jibAngle);
  drawTelltales(startPos, PORT, jibAngle);
}

void resetWindParticle (WindParticle *w) {
  if (GetRandomValue(0, 100) < 50) {
    if (windDir.x > 0) w->pos.x = -64;
    if (windDir.x < 0) w->pos.x = GetScreenWidth() + 64;
    w->pos.y = GetRandomValue(-64, GetScreenHeight() + 64);
  } else {
    if (windDir.y > 0) w->pos.y = -64;
    if (windDir.y < 0) w->pos.y = GetScreenHeight() + 64;
    w->pos.x = GetRandomValue(-64, GetScreenWidth() + 64);
  }
}

void initWindParticles (void) {
  for (int i=0; i < MAX_WIND_PARTICLES; i++) {
    wind[i].pos.x = GetRandomValue(-64, GetScreenWidth() + 64);
    wind[i].pos.y = GetRandomValue(-64, GetScreenHeight() + 64);
  }
}

void updateWindParticles (Camera2D camera, Vector2 cameraVel) {
  Vector2 step = vscale(vsub(windDir, cameraVel), SCALE_M_TO_PX * 2);
  step = vscale(step, camera.zoom);
  for (int i=0; i < MAX_WIND_PARTICLES; i++) {
    wind[i].pos.x += step.x * DT;
    wind[i].pos.y += step.y * DT;

    if (wind[i].pos.x < -64 || wind[i].pos.x > GetScreenWidth()+64 ||
        wind[i].pos.y < -64 || wind[i].pos.y > GetScreenHeight()+64) {
      resetWindParticle(&wind[i]);
    }
  }
}

void drawWindParticles (Vector2 cameraVel) {
  int toUse = vlen(windDir) + 10;
  Vector2 tailStep = vscale(vsub(windDir, cameraVel), SCALE_M_TO_PX * 0.01f);
  tailStep = vscale(tailStep, vlen(tailStep)*0.2f+1.5f);

  for (int i=0; i < toUse; i++) {
    Vector2 end = vsub(wind[i].pos, tailStep);
    unsigned char alpha = 32;
    DrawLineEx(wind[i].pos, end, 2.0f, (Color){255, 255, 255, alpha});

    end = vsub(end, tailStep);
    alpha = (alpha * 2) / 3;
    DrawLineEx(wind[i].pos, end, 2.0f, (Color){255, 255, 255, alpha});

    end = vsub(end, tailStep);
    alpha = (alpha * 2) / 3;
    DrawLineEx(wind[i].pos, end, 2.0f, (Color){255, 255, 255, alpha});

    end = vsub(end, tailStep);
    alpha = (alpha * 2) / 3;
    DrawLineEx(wind[i].pos, end, 2.0f, (Color){255, 255, 255, alpha});
  }
}

void initBubbleParticles (void) {
  for (int i=0; i < MAX_BUBBLE_PARTICLES; i++) {
    bubbles[i].lifetime = 0;
  }
}

void updateBubbleParticles () {
  for (int i=0; i < MAX_BUBBLE_PARTICLES; i++) {
    if (bubbles[i].lifetime <= 0) continue;
    bubbles[i].pos.x += bubbles[i].vel.x * DT;
    bubbles[i].pos.y += bubbles[i].vel.y * DT;
    bubbles[i].vel.x *= 0.9998f;
    bubbles[i].vel.y *= 0.9998f;
    bubbles[i].lifetime--;
  }
}

void drawBubbleParticles () {
  BeginBlendMode(BLEND_ALPHA);
  for (int i=0; i < MAX_BUBBLE_PARTICLES; i++) {
    if (bubbles[i].lifetime > 0) {
      Color rgba = { 200, 200, 255, bubbles[i].lifetime * 128 / BUBBLE_LIFETIME };
      float radius = (1.0f - bubbles[i].lifetime / BUBBLE_LIFETIME) * 4.0f + 1.5f;
      DrawCircle(bubbles[i].pos.x * SCALE_M_TO_PX,
                 bubbles[i].pos.y * SCALE_M_TO_PX, radius, rgba);
    }
  }
  EndBlendMode();
}

int getFreeBubbleIdx (void) {
  for (int i=0; i < MAX_BUBBLE_PARTICLES; i++) {
    if (bubbles[i].lifetime == 0) return i;
  }
  return -1;
}

void spawnBubbles (Vector2 at, int num, float radius) {
  for (int i=0; i < num; i++) {
    int j = getFreeBubbleIdx();
    if (j == -1) return;
    bubbles[j].lifetime = BUBBLE_LIFETIME;
    float theta = randf(0, 360);
    bubbles[j].pos = vadd(at, vdir(theta, randf(0, radius)));
    bubbles[j].vel = vdir(theta, randf(0, 0.85f));
  }
}


void applyForce (Vessel *v, Vector2 fN) {
  v->vel = vadd(v->vel, vscale(fN, 1.0f / v->massKg));
}

float computeSailEfficiency (float theta, float trim) {
  while (theta < 0) theta += 360;
  while (theta > 360) theta -= 360;
  if (theta > 180) theta = 360 - theta;

  if (theta < 30) return 0;

  float optimal = 1 - (theta - 30) / (180 - 30);
  if (theta >= 150) optimal = 0;

  const float diff = fabs(trim - optimal);
  const float eff = -(diff * 2.1)*(diff * 2.1) + 1;
  return fmaxf(0.05, eff);
}

float computeSailForce (float windSpeed, float theta, float eff) {
  return log10(windSpeed + 1) * 4.7f * eff * 1.0f;
}

int main(void)
{
    InitWindow(800, 600, "Segeln Sie! :: v0.1.0");

    Texture2D water = LoadTexture("water.png");
    Texture2D ship = LoadTexture("dufour-31.png");

    Camera2D camera = { 0 };
    camera.offset = (Vector2){ GetScreenWidth()/2, GetScreenHeight()/2 };
    camera.rotation = 0.0f;
    camera.zoom = 0.7f;
    camera.target.x = 0;

    Vessel boat;
    boat.rot = randf(0, 360);
    boat.pivotX = 1 / 3.0f;
    boat.pos = v(0, 0);
    boat.vel = v(0, 0);
    boat.massKg = 4535.92f;
    boat.mainHardening = 0.5f;
    boat.jibHardening = 0.5f;

    windDir = vdir(randf(0,360), randf(2, 15));
    initWindParticles();

    while (!WindowShouldClose())
    {
        boat.pos = vadd(boat.pos, vscale(boat.vel, DT));
        camera.target = vsub(vscale(boat.pos, SCALE_M_TO_PX),
                             v(ship.width/4, ship.height/4));
        updateWindParticles(camera, boat.vel);
        updateBubbleParticles();

        // Apply mainsail force.
        float theta = vang(windDir) - (boat.rot - 180);
        float mainEff = computeSailEfficiency(theta, boat.mainHardening);
        float mainForceMagnitude = computeSailForce(vlen(windDir), theta, mainEff);
        mainForceMagnitude *= 0.73f * 2;
        applyForce(&boat, vdir(boat.rot, mainForceMagnitude));

        // Apply jib force.
        float jibEff = computeSailEfficiency(theta, boat.jibHardening);
        float jibForceMagnitude = computeSailForce(vlen(windDir), theta, jibEff);
        jibForceMagnitude *= 0.27f * 2;
        applyForce(&boat, vdir(boat.rot, jibForceMagnitude));

        // Apply water drag.
        Vector2 drag = vscale(boat.vel, -4.0f);
        applyForce(&boat, drag);

        // Spawn bubbles, if moving.
        if (vlen(boat.vel) > 0.1f) {
          if (rand(0,200)<=1) {
            spawnBubbles(vadd(boat.pos, vdir(boat.rot-180, 4.1f)), 1, 0.5f);
          }
        }

        BeginDrawing();
          ClearBackground(BLACK);
          drawWater(&water, camera);

          BeginMode2D(camera);
            drawBubbleParticles();
            drawShip(&ship, &boat);
          EndMode2D();

          drawWindParticles(boat.vel);

          char str[32];
          sprintf(str, "VESSEL SPEED: %.2f knots", vlen(boat.vel) * MS_TO_KNOTS);
          DrawText(str, GetScreenWidth()-325, 10, 20, LIGHTGRAY);

          sprintf(str, "TRUE WIND: %.2f knots", vlen(windDir) * MS_TO_KNOTS);
          DrawText(str, GetScreenWidth()-325, 40, 20, LIGHTGRAY);
        EndDrawing();

        if (IsKeyDown(KEY_RIGHT)) {
          boat.rot += 0.019f;
        }
        if (IsKeyDown(KEY_LEFT)) {
          boat.rot -= 0.019f;
        }

        if (IsKeyDown(KEY_M)) {
          boat.mainHardening = fminf(boat.mainHardening + 0.0001f, 1.0f);
        }
        if (IsKeyDown(KEY_N)) {
          boat.mainHardening = fmaxf(boat.mainHardening - 0.0002f, 0.0f);
        }

        if (IsKeyDown(KEY_J)) {
          boat.jibHardening = fminf(boat.jibHardening + 0.0001f, 1.0f);
        }
        if (IsKeyDown(KEY_H)) {
          boat.jibHardening = fmaxf(boat.jibHardening - 0.0002f, 0.0f);
        }

        if (IsKeyDown(KEY_EQUAL)) {
          camera.zoom += 0.001f;
        }
        if (IsKeyDown(KEY_MINUS)) {
          camera.zoom -= 0.001f;
        }
    }

    UnloadTexture(ship);
    UnloadTexture(water);

    CloseWindow();

    return 0;
}

